package activity.code.source.es.scos;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.source.code.model.Food;


/**
 * A simple {@link Fragment} subclass.
 */
public class HotFoodFragment extends BaseFoodFragment {

    @Override
    public void initFoods() {
        foodlist.clear();
        for (int i = 0;i < 2;i++){
            Food hotdish_1 = new Food(R.drawable.ic_hotdish_1,"28","红烧排骨");
            foodlist.add(hotdish_1);
            Food hotdish_2 = new Food(R.drawable.ic_hotdish_2,"33","红烧鲫鱼");
            foodlist.add(hotdish_2);
            Food hotdish_3 = new Food(R.drawable.ic_hotdish_3,"29","笼蒸肉丸");
            foodlist.add(hotdish_3);
        }
    }

}
