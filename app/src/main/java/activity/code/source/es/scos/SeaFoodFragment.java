package activity.code.source.es.scos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.source.code.model.Food;


public class SeaFoodFragment extends BaseFoodFragment {
    @Override
    public void initFoods() {
        foodlist.clear();
        for (int i = 0;i < 2;i++){
            Food seefood_1 = new Food(R.drawable.ic_seafood_1,"40","爆炒海螺");
            foodlist.add(seefood_1);
            Food seefood_2 = new Food(R.drawable.ic_seafood_2,"45","海鲜杂烩");
            foodlist.add(seefood_2);
            Food seefood_3 = new Food(R.drawable.ic_seafood_3,"40","基围虾");
            foodlist.add(seefood_3);
            Food seefood_4 = new Food(R.drawable.ic_seafood_4,"38","粉丝扇贝");
            foodlist.add(seefood_4);
        }
    }
}
