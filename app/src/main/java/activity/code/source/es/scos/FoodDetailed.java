package activity.code.source.es.scos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import es.source.code.model.Food;

public class FoodDetailed extends AppCompatActivity implements View.OnTouchListener,GestureDetector.OnGestureListener {

    private ArrayList<Food> foodArrayList;
    private int index;
    private ImageView detail_image;
    private TextView detail_name;
    private TextView detail_price;
    private EditText detail_info;
    private GestureDetector gd;
    private Button detail_btn;
    LinearLayout view;
    FoodUtils foodUtils = new FoodUtils();
    List<Food> foods;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_detailed);

        detail_image = (ImageView) findViewById(R.id.detail_image);
        detail_name = (TextView) findViewById(R.id.detail_name);
        detail_price = (TextView) findViewById(R.id.detail_price);
        detail_info = (EditText) findViewById(R.id.detail_info);
        detail_btn = (Button) findViewById(R.id.detail_btn);
        Intent intent = getIntent();
        foodArrayList = (ArrayList<Food>) intent.getSerializableExtra("foodlist");
        index = (int) intent.getIntExtra("index", 0);
        detail_image.setImageResource(foodArrayList.get(index).getImageView());
        detail_name.setText("菜名：" + foodArrayList.get(index).getName());
        detail_price.setText("价格：" + foodArrayList.get(index).getPrice());
        foods = foodUtils.getFoodList();
        detail_btn.setText("点菜");
        for (Food i:
                foods) {
            if(i.getImageView() == foodArrayList.get(index).getImageView()){
                detail_btn.setText("退点");
            }
        }
        detail_btn.setOnClickListener(new View.OnClickListener() {
            Button btn = detail_btn;
            @Override
            public void onClick(View v) {
                if(btn.getText().equals("退点")){
                    ActivityUtils.showShortToast(FoodDetailed.this,"退点成功");
                    foodUtils.removeNotOrderFood(foodArrayList.get(index));
//                    foodUtils.clearNotOrderFood();
                    btn.setText("点菜");
                }else{
                    ActivityUtils.showShortToast(FoodDetailed.this,"点菜成功");
                    foodUtils.addNotOrderFood(foodArrayList.get(index));
                    btn.setText("退点");
                }
            }
        });

        view = (LinearLayout) findViewById(R.id.detail_view);
        view.setOnTouchListener(this);
        view.setLongClickable(true);
        gd = new GestureDetector((GestureDetector.OnGestureListener) this);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        //设置最小的手势移动距离和手势移动速度
        final int FLING_MIN_DISTANCE = 100;
        final int FLING_MIN_VELOCITY = 200;

        //向右的手势
        if (e2.getX() - e1.getX() > FLING_MIN_DISTANCE && Math.abs(velocityX) > FLING_MIN_VELOCITY) {
            if (index > 0) {
                index = index - 1;

            } else {
                index = 0;
                Toast.makeText(this, "已翻到最开始", Toast.LENGTH_SHORT).show();
            }
            foods = foodUtils.getFoodList();
            detail_btn.setText("点菜");
            for (Food i:
                    foods) {
                if(i.getImageView() == foodArrayList.get(index).getImageView()){
                    detail_btn.setText("退点");
                }
            }
            detail_image.setImageResource(foodArrayList.get(index).getImageView());
            detail_name.setText("菜名：" + foodArrayList.get(index).getName());
            detail_price.setText("价格：" + foodArrayList.get(index).getPrice());
        }
        // 向左的手势
        if (e1.getX() - e2.getX() > FLING_MIN_DISTANCE && Math.abs(velocityX) > FLING_MIN_VELOCITY) {
            int len = foodArrayList.size();
            if (index < len - 1) {
                index = index + 1;

            } else {
                index = len -1;
                Toast.makeText(this, "已翻到最末", Toast.LENGTH_SHORT).show();
            }

            foods = foodUtils.getFoodList();
            detail_btn.setText("点菜");
            for (Food i:
                    foods) {
                if(i.getImageView() == foodArrayList.get(index).getImageView()){
                    detail_btn.setText("退点");
                }
            }
            detail_image.setImageResource(foodArrayList.get(index).getImageView());
            detail_name.setText("菜名：" + foodArrayList.get(index).getName());
            detail_price.setText("价格：" + foodArrayList.get(index).getPrice());
        }
        return false;
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return gd.onTouchEvent(event);
    }
}